Je veux implémenter un programme qui permet de simuler une feuille de score pour le jeu Qwixx. 

Je veux utiliser React avec vite.js. J'ai déjà initialisé le projet, tu n'as pas à le faire. 
Je veux que tu sépares le plus possible en petits composants réutilisables. 
Je veux tout le code dans un seul fichier App.jsx, donc tous les composants dans ce fichier, et tout le css dans App.css. Ne met donc pas les composants dans des fichiers séparés. 

Je ne veux pas que l'on ait à scroller ni verticalement ni horizontalement. 

Si c'est ok pour toi je vais continuer en te décrivant l'IHM. 
Ensuite je te décrirais les règles métiers. 

---------------------------------------------------
La feuille de score doit prendre toute la hauteur de l'écran. 

La feuille de score est composée de 5 lignes. 

Premièrement une ligne avec un fond rouge composée d'abord de 11 cases, chacune avec un numéro en son centre. 
La case est de la couleur de la ligne (donc rouge).
Le numéro au centre de la case est en noir. Le numéro est entièrement entouré par un cercle rempli de blanc. Les cases sont numérotées de 2 à 12. 
A la fin de la ligne il y a une case (rouge aussi), avec un symbole "verrou" entouré par un cercle rempli de blanc. 
La ligne prend toute la largeur de l'écran. 

Dans un premier temps tu vas remettre copier cette ligne 4 fois afin d'avoir 5 lignes les unes en dessous des autres. 
-------------------------------------------------------

Maintenant tu vas remplacer la deuxième ligne par une autre ligne. 

Cette ligne est avec un fond jaune composée d'abord de 11 cases, chacune avec un numéro en son centre. 
La case est de la couleur de la ligne (donc jaune).
Le numéro au centre de la case est en noir. Le numéro est entièrement entouré par un cercle rempli de blanc. Les cases sont numérotées de 2 à 12. 
A la fin de la ligne il y a une case (jaune aussi), avec un symbole "verrou" entouré par un cercle rempli de blanc. 
La ligne prend toute la largeur de l'écran. 

------------------------------------------------------
Pour modifier la troisième ligne : tu vas remplacer la couleur par du vert, et numéroter les case de 12 jusqu'au 2 (donc en descendant) au lieu du 2 au 12. 

------------------------------------------------------

Pour modifier la quatrième ligne : c'est comme la troisième ligne sauf que la couleur est bleue

------------------------------------------------------
La dernière ligne tu va la remplacer par une ligne qui va d'abord avoir 4 cases avec un fond blanc et un bord épais grisé. Ce seront des cases pour indiquer un coup manqué. 
Ensuite tu ajoutes 4 cases avec un fond blanc et un bord rouge pour la première case, jaune pour la deuxième case, vert pour la troisième et bleu pour la quatrième. Ces 4 cases représentent le score des lignes précédentes pour leur couleur respective. 
Ensuite tu ajoutes une case avec un bord noir pour le score total. 
Enfin tu ajoutes deux cases qui seront des boutons. La première avec une icone de recylcage pour pouvoir recommencer une partie, et une autre avec une icone de fleche vert l'arrière afin de pouvoir revenir un coup en arrière (undo). 
Toutes les cases de cette ligne sont aussi les unes à côté des autres et doivent tenir sur une seule ligne de l'écran.
------------------------------------------------------

Pour rappel ces 5 lignes forment la feuille de score et doivent prendre toute la hauteur de l'écran, sans qu'il y ait d'espace entre chaque ligne. 
Chaque ligne doit avoir toute ses cases les unes à côté des autres. 

Le symboles verrou doit être issu de font-awsome.

Avant de décrire la logique métier, les règles, est-ce que tu as tout ce qu'il faut pour réaliser le développement de l'IHM ? 
Si oui, alors donne moi le code complet de tous les éléments décrits. 
