# Ubiquitous language
* Joueur
* Fiche
* Rangée
* Couleur (des rangées)
* Chiffre (ou numéro)
* Croix (ou cocher)
* Dés
* Action
* Joueur actif
* Cadenas
* Clôture d'une rangée
* Coups manqués
* Décompte
* Points
* Gagnant
* Sens horaire
* Lance les dés
* Résultat des dés
* Annonce du résultat
* Coup manqué
* Retirer du jeu

# Les règles métiers utiles
* DONE - Le joueur peut cocher les chiffres dans les quatre rangées de couleur de sa fiche.
* DONE - Les chiffres doivent être cochés de gauche à droite dans chaque rangée de couleur.
* DONE - Le joueur peut sauter des chiffres dans une rangée, mais une fois ignorés, ils ne peuvent plus être cochés par la suite.
* DONE - Si le joueur ne coche aucun chiffre dans une action, il doit faire une croix dans une case "coups manqués".
* DONE - Lorsqu'un joueur souhaite cocher le dernier chiffre d'une rangée, il doit avoir coché au moins cinq chiffres dans cette rangée.
* DONE - Une rangée clôturée ne peut plus être cochée par aucun joueur.
* DONE - Si un joueur coche moins de cinq chiffres dans une rangée, il ne peut en aucun cas cocher le cadenas de cette rangée même si elle est clôturée.
* DONE - La partie se termine immédiatement si un joueur a coché la quatrième case "coups manqués".
* DONE - quand la partie est terminée je ne peux plus faire de coup manqué
* DONE - La partie se termine immédiatement lorsque deux rangées sont clôturées.  
* DONE - quand la partie est terminée je ne peux plus cloturer une rangée
* DONE - si la partie est terminée on ne peut plus cocher

# Faire une UI qui utilise l'objet métier
=> voir avec ChatGPT aussi