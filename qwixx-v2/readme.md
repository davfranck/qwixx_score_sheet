npm create vite@latest qwixx-v2 --template react
cd qwixx-v2
npm install
npm run dev

Installer vitest : `npm install -D vitest`. 
Documentation api vitest : https://vitest.dev/api/#test. 

MDN Javascript : https://developer.mozilla.org/fr/docs/Web/JavaScript

# TODO
* undo : c'est crado ce que j'ai fait. 
  * il faudrait faire un obet Fiche immutable
  * voir aussi comment rendre le reducer pur
* on pourrait faire plus propre en mettant du comportement spécifique UI sur le viewModel (les 'checked' par ex)
* on peut cocher avec undefined sur le modèle : faire un check de la valeur passée en paramètre
