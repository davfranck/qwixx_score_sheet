import "./App.css";
import { useReducer } from "react";
import { Fiche, convertRowName } from "./model/Fiche";

const ScoreCell = ({ color, num, checked, onCheck }) => {
    const onClickHandler = () => {
        if (!checked) {
            onCheck(num);
        }
    };
    return (
        <div className={`score-cell ${color}`} onClick={onClickHandler}>
            <div className={`score-number ${checked ? "checked" : ""}`}>
                {num}
            </div>
        </div>
    );
};

const LockCell = ({ color, checked }) => {
    return (
        <div className={`score-cell ${color}`}>
            <div className={`lock-symbol ${checked ? "checked" : ""}`}>
                &#128274;️
            </div>
        </div>
    );
};

const ScoreRow = ({ color, reverse, data, dispatch }) => {
    const numbers = [...Array(11).keys()].map((num) => num + 2);
    if (reverse) {
        numbers.reverse();
    }

    const doCheck = (num) => {
        dispatch({ type: "check", color, value: num });
    };

    return (
        <div className="score-row">
            {numbers.map((num) => (
                <ScoreCell
                    color={color}
                    num={num}
                    key={num}
                    checked={data.values.includes(num)}
                    onCheck={doCheck}
                />
            ))}
            <LockCell color={color} checked={data.lastNumberIsChecked} />
        </div>
    );
};

const MissedTurnCell = ({ number, dispatch, checked }) => {
    const onMiss = () => {
        if (!checked) {
            dispatch({ type: "miss", value: number });
        }
    };
    return (
        <div
            className={`missed-turn-cell ${checked ? "checked" : ""}`}
            onClick={onMiss}
        ></div>
    );
};

const ScoreCellByColor = ({ color, value }) => {
    return <div className={`score-cell-by-color ${color}`}>{value}</div>;
};

const TotalScoreCell = ({ total }) => {
    return <div className="total-score-cell">{total}</div>;
};

const ControlButton = ({ symbol, action, dispatch }) => {
    const onClickHandler = () => {
        let doAction = true;
        if(action === "newGame") {
            doAction = confirm("Nouvelle partie ?");
        }
        if (doAction) {
            dispatch({ type: action });
        }
    };
    return (
        <div className="control-button" onClick={onClickHandler}>
            {symbol}
        </div>
    );
};

const ControlRow = ({ sheet, dispatch }) => {
    return (
        <div className="control-row">
            {[...Array(4).keys()].map((num) => (
                <MissedTurnCell
                    number={num + 1}
                    key={num}
                    dispatch={dispatch}
                    checked={num < sheet.missed}
                />
            ))}
            {["red", "yellow", "green", "blue"].map((color) => (
                <ScoreCellByColor
                    color={color}
                    key={color}
                    value={sheet[color].score}
                />
            ))}
            <TotalScoreCell total={sheet.total} />
            <ControlButton
                action="newGame"
                symbol="&#127922;"
                dispatch={dispatch}
            />
            <ControlButton action="undo" symbol="&#9100;" dispatch={dispatch} />
        </div>
    );
};

const mapRangee = (rangee) => ({
    score: rangee.points(),
    values: rangee.values,
    lastNumberIsChecked: rangee.lastNumberIsChecked(),
});
const mapFiche = (fiche) => {
    return {
        gameOver: fiche.partieTerminee,
        red: mapRangee(fiche.rangeeParNom("red")),
        yellow: mapRangee(fiche.rangeeParNom("yellow")),
        blue: mapRangee(fiche.rangeeParNom("blue")),
        green: mapRangee(fiche.rangeeParNom("green")),
        total: fiche.points(),
        missed: fiche.nbCoupManque,
    };
};
const reducer = (state, action) => {
    switch (action.type) {
        case "check":
            memo.push(fiche)
            fiche = fiche.copy()
            console.log(
                `check couleur ${action.color} et valeur ${action.value}`
            );
            fiche.cocher(convertRowName(action.color), action.value);
            break;
        case "miss":
            memo.push(fiche)
            fiche = fiche.copy()
            console.log("check miss before + 1: " + fiche.nbCoupManque);
            fiche.coupManque(action.value);
            break;
        case "newGame":
            fiche = new Fiche();
            memo = [];
            break;
        case "undo": 
            fiche = memo.pop();
            break;
    }
    const sheet = mapFiche(fiche);
    console.log("sheet mappée en sortie du reducer");
    console.log(sheet);
    console.log("memo");
    console.log(memo);
    return sheet;
};
let fiche = new Fiche();
let memo = [];

const ScoreSheet = () => {
    const [sheet, dispatch] = useReducer(reducer, mapFiche(fiche));
    console.log("Sheet dans le composant principal");
    console.log(sheet);
    return (
        <div className="score-sheet">
            <ScoreRow color="red" data={sheet.red} dispatch={dispatch} />
            <ScoreRow color="yellow" data={sheet.yellow} dispatch={dispatch} />
            <ScoreRow
                color="green"
                reverse
                data={sheet.green}
                dispatch={dispatch}
            />
            <ScoreRow
                color="blue"
                reverse
                data={sheet.blue}
                dispatch={dispatch}
            />
            <ControlRow sheet={sheet} dispatch={dispatch} />
        </div>
    );
};

function App() {
    return (
        <div className="App">
            <ScoreSheet />
        </div>
    );
}

export default App;
