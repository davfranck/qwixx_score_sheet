export class Fiche {
    constructor(
        rouge = new RangeeCroissante(couleurs.ROUGE, "red"),
        jaune = new RangeeCroissante(couleurs.JAUNE, "yellow"),
        bleue = new RangeeDecroissante(couleurs.BLEUE, "blue"),
        verte = new RangeeDecroissante(couleurs.VERTE, "green"),
        partieTerminee = false,
        nbCoupManque = 0
    ) {
        this.rouge = rouge;
        this.jaune = jaune;
        this.bleue = bleue;
        this.verte = verte;
        this.rangees = [this.rouge, this.jaune, this.verte, this.bleue];
        this.partieTerminee = partieTerminee;
        this.nbCoupManque = nbCoupManque;
    }
    copy() {
        return new Fiche(
            this.rouge.copy(),
            this.jaune.copy(),
            this.bleue.copy(),
            this.verte.copy(),
            this.partieTerminee,
            this.nbCoupManque
        )
    }
    points() {
        return (
            this.bleue.points() +
            this.rouge.points() +
            this.jaune.points() +
            this.verte.points() -
            this.nbCoupManque * 5
        );
    }

    cocher(couleur, chiffre) {
        if (this.partieTerminee) {
            return false;
        }
        return this.rangee(couleur).cocher(chiffre);
    }
    cloturer(couleur) {
        if (this.partieTerminee) {
            return false;
        }
        this.rangee(couleur).cloturer();
        const nbRangeesFermees = this.rangees.filter(
            (rangee) => rangee.close
        ).length;
        if (nbRangeesFermees == 2) {
            this.partieTerminee = true;
        }
        return true;
    }
    coupManque(nb) {
        if (this.nbCoupManque == 4) {
            return false;
        }
        if (nb == this.nbCoupManque) {
            return false;
        }
        this.nbCoupManque++;
        if (this.nbCoupManque === 4) {
            this.partieTerminee = true;
        }
        return true;
    }

    rangee(couleur) {
        switch (couleur) {
            case couleurs.ROUGE:
                return this.rouge;
            case couleurs.JAUNE:
                return this.jaune;
            case couleurs.BLEUE:
                return this.bleue;
            case couleurs.VERTE:
                return this.verte;
        }
    }
    rangeeParNom(couleur) {
        return this.rangees.find((rangee) => rangee.nom === couleur);
    }
}

class Rangee {
    pointsParNombre = [0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78];
    constructor(couleur, nom, values = [], close = false) {
        this.couleur = couleur;
        this.nom = nom;
        this.values = values;
        this.close = close;
    }
    isChecked(chiffre) {
        return this.values.find((it) => it === chiffre) != undefined;
    }
    points() {
        let bonus = 0;
        if (this.lastNumberIsChecked()) {
            bonus = 1;
        }
        return this.pointsParNombre[this.values.length + bonus];
    }
    cloturer() {
        this.close = true;
    }
}

class RangeeCroissante extends Rangee {
    cocher(chiffre) {
        if (
            this.close ||
            this.chiffreProposeDejaIgnore(chiffre) ||
            this.chiffreEst12EtPasEncore5CasesCochees(chiffre)
        ) {
            return false;
        }
        this.values.push(chiffre);
        return true;
    }
    chiffreProposeDejaIgnore(chiffre) {
        return this.values[this.values.length - 1] >= chiffre;
    }
    chiffreEst12EtPasEncore5CasesCochees(chiffre) {
        return this.values.length < 5 && chiffre === 12;
    }
    lastNumberIsChecked() {
        return this.values[this.values.length - 1] == 12;
    }
    copy(){
        return new RangeeCroissante(this.couleur, this.nom, [...this.values], this.close);
    }
}
class RangeeDecroissante extends Rangee {
    cocher(chiffre) {
        if (
            this.close ||
            this.chiffreProposeDejaIgnore(chiffre) ||
            this.chiffreEst2EtPasEncore5CasesCochees(chiffre)
        ) {
            return false;
        }
        this.values.push(chiffre);
        return true;
    }
    chiffreProposeDejaIgnore(chiffre) {
        return this.values[this.values.length - 1] <= chiffre;
    }
    chiffreEst2EtPasEncore5CasesCochees(chiffre) {
        return this.values.length < 5 && chiffre === 2;
    }
    lastNumberIsChecked() {
        return this.values[this.values.length - 1] == 2;
    }
    copy(){
        return new RangeeDecroissante(this.couleur, this.nom, [...this.values], this.close);
    }
}

export const couleurs = { ROUGE: 0, JAUNE: 1, BLEUE: 2, VERTE: 3 };

export const convertRowName = (name) => {
    switch (name) {
        case "red":
            return 0;
        case "yellow":
            return 1;
        case "blue":
            return 2;
        case "green":
            return 3;
    }
};
