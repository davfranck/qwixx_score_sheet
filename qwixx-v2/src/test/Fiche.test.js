import { describe, expect, test } from 'vitest'
import { Fiche, couleurs } from '../model/Fiche'

test('Test création parie', () => {
    expect(new Fiche().partieTerminee).toBeFalsy()
})

describe('On peut cocher un chiffre sur une rangée', () => {
    test.each([
        [couleurs.ROUGE, 2],
        [couleurs.VERTE, 3],
        [couleurs.BLEUE, 5],
        [couleurs.JAUNE, 4]
    ])('Sur la rangée %i on peut cocher %i', (couleur, chiffre) => {
        const fiche = new Fiche()
        fiche.cocher(couleur, chiffre)
        expect(fiche.rangee(couleur).isChecked(chiffre)).toBeTruthy()
    })
})

describe('je ne peux pas cocher un chiffre avant un chiffre coché', () => {
    test.each([
        { rangee: couleurs.ROUGE, first: 4, second: 2 },
        { rangee: couleurs.JAUNE, first: 6, second: 3 },
        { rangee: couleurs.BLEUE, first: 10, second: 11 },
        { rangee: couleurs.VERTE, first: 9, second: 10 },
    ])('Sur la rangée $rangee si je coche d\'abord le chiffre $first je ne peux pas cocher le chiffre $second', ({ rangee, first, second }) => {
        const fiche = new Fiche();
        fiche.cocher(rangee, first)
        expect(fiche.cocher(rangee, second)).toBe(false)
    })
})

describe('calcul du score total', () => {
    test('le score est de 0 au départ', () => {
        const fiche = new Fiche()
        expect(fiche.points()).toEqual(0)
    })
    test.each([
        [5,couleurs.BLEUE],
        [5,couleurs.VERTE],
        [5,couleurs.ROUGE],
        [5,couleurs.JAUNE],
    ])('le score est de 1 point quand je coche le chiffre %i dans la rangée %i', (chiffre, rangee) => {
        const fiche = new Fiche()
        fiche.cocher(rangee, chiffre)
        expect(fiche.points()).toEqual(1);
    })
    test('le score est le total des points de chaque rangées', () => {
        const fiche = new Fiche();
        fiche.cocher(couleurs.BLEUE, 10)
        fiche.cocher(couleurs.VERTE, 10)
        fiche.cocher(couleurs.ROUGE, 10)
        fiche.cocher(couleurs.JAUNE, 10)
        expect(fiche.points()).toEqual(4)
    })
    test('plusieurs points dans une rangée', () => {
        const fiche = new Fiche()
        fiche.cocher(couleurs.ROUGE, 2)
        fiche.cocher(couleurs.ROUGE, 3)
        expect(fiche.points()).toEqual(3)
    })
    test.each([
        [couleurs.ROUGE],
        [couleurs.JAUNE]
    ])('bonus +1 case cochée quand on a coché la dernière case de la rangée %i', (couleur) => {
        const fiche = new Fiche()
        fiche.cocher(couleur, 2);
        fiche.cocher(couleur, 8);
        fiche.cocher(couleur, 9);
        fiche.cocher(couleur, 10);
        fiche.cocher(couleur, 11);
        fiche.cocher(couleur, 12);
        expect(fiche.points()).toEqual(28);
    })
    test.each([
        [couleurs.BLEUE],
        [couleurs.VERTE]
    ])('bonus +1 case cochée quand on a coché la dernière case de la rangée %i', (couleur) => {
        const fiche = new Fiche()
        fiche.cocher(couleur, 12);
        fiche.cocher(couleur, 8);
        fiche.cocher(couleur, 7);
        fiche.cocher(couleur, 6);
        fiche.cocher(couleur, 5);
        fiche.cocher(couleur, 2);
        expect(fiche.points()).toEqual(28);
    })
})

describe('coup manqué', () => {
    test('Coup manqué', () => {
        const fiche = new Fiche()
        fiche.coupManque(1);
        expect(fiche.points()).toBe(-5)
    })
    test('coup manqué deux fois le même numéro', () => {
        const fiche = new Fiche();
        fiche.coupManque(1);
        expect(fiche.coupManque(1)).toBe(false);
    })
})

test.each([
    [couleurs.ROUGE],
    [couleurs.JAUNE],
])('je ne peux pas cocher le 12 sur les rangées rouge et jaune si je n\'ai pas coché 5 chiffres', (couleur) => {
    const fiche = new Fiche();
    expect(fiche.cocher(couleur, 12)).toBe(false);
})
test.each([
    [couleurs.BLEUE],
    [couleurs.VERTE],
])('je ne peux pas cocher le 2 sur les rangées bleue et verte si je n\'ai pas coché 5 chiffres', (couleur) => {
    const fiche = new Fiche();
    expect(fiche.cocher(couleur, 2)).toBe(false);
})
test.each([
    [couleurs.ROUGE],
    [couleurs.JAUNE],
    [couleurs.BLEUE],
    [couleurs.VERTE]
])('La rangée %i clôturée ne peut plus être cochée par aucun joueur', (couleur) => {
    const fiche = new Fiche()
    fiche.cloturer(couleur);
    expect(fiche.cocher(couleur, 4)).toBe(false)
})
describe('tests de fin de partie', () => {
        test('La partie se termine immédiatement si un joueur a coché la quatrième case "coups manqués', () => {
            const fiche = new Fiche()
            fiche.coupManque(1)
            fiche.coupManque(2)
            fiche.coupManque(3)
            fiche.coupManque(4)
            expect(fiche.partieTerminee).toBeTruthy()
        })
        test('quand la partie est terminé je ne peux plus faire de coup manqué', () => {
            const fiche = new Fiche()
            fiche.coupManque(1)
            fiche.coupManque(2)
            fiche.coupManque(3)
            fiche.coupManque(4)
            expect(fiche.coupManque(5)).toBe(false)
        })
        test('La partie se termine immédiatement lorsque deux rangées sont clôturées.', () => {
            const fiche = new Fiche()
            fiche.cloturer(couleurs.ROUGE)
            fiche.cloturer(couleurs.JAUNE)
            expect(fiche.partieTerminee).toBeTruthy()
        })
        test('quand la partie est terminée je ne peux plus cloturer une rangée', () => {
            const fiche = new Fiche()
            fiche.cloturer(couleurs.ROUGE)
            fiche.cloturer(couleurs.JAUNE)
            expect(fiche.cloturer(couleurs.BLEUE)).toBe(false)
        })
        test('quand la partie est terminée je ne peux plus cocher', () => {
            const fiche = new Fiche();
            fiche.cloturer(couleurs.ROUGE)
            fiche.cloturer(couleurs.JAUNE)
            expect(fiche.cocher(couleurs.BLEUE, 5)).toBe(false)
        })
})
test('recup rangee par couleur', () => {
    const fiche = new Fiche()
    expect(fiche.rangeeParNom('red').nom).toEqual('red')
    expect(fiche.rangeeParNom('yellow').nom).toEqual('yellow')
    expect(fiche.rangeeParNom('blue').nom).toEqual('blue')
    expect(fiche.rangeeParNom('green').nom).toEqual('green')
})