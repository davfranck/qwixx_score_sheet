# Description
Chaque joueur tente de cocher le plus grand nombre de chiffres dans les quatre rangées de couleur de sa fiche. 
Plus il y a de croix dans une rangée, plus il récoltera de points. 
Le joueur comptabilisant le plus de points à l’issue de la partie est le gagnant.

Au cours de la partie, les chiffres de chaque rangée de couleur doivent être cochés de gauche à droite. 
On ne doit toutefois pas commencer tout à gauche – il est possible de sauter quelques chiffres (plusieurs de suite si souhaité). 
Mais les chiffres ignorés ne pourront plus être cochés par la suite.
Remarque : on peut barrer les chiffres ignorés avec un trait horizontal, afin qu’ils ne soient pas cochés par erreur au cours de la partie.

Exemple :
Dans la rangée rouge : le 5 a été coché avant le 7. 
Les chiffres 2, 3, 4 et 6 ne pourront plus être cochés ultérieurement.
Dans la rangée jaune : seuls le 11 et le 12 peuvent encore être cochés.
Dans la rangée verte : on ne peut que cocher les chiffres après le 6. Dans la rangée bleue : on doit continuer à partir du 10.

## Déroulement de la partie
Chaque joueur reçoit un crayon et une fiche. 
Le joueur actif – celui qui lancera les dés – est tiré au sort. 
Le joueur actif lance les six dés en même temps. 
Les joueurs procèdent ensuite aux deux actions suivantes l’une après l’autre :
1. Le joueur actif additionne les points des deux dés blancs et annonce le résultat de façon claire et intelligible.
Chaque joueur peut (mais ne doit pas !) cocher le chiffre annoncé dans la rangée de la couleur de son choix.
Exemple : Max est le joueur actif. Les deux dés blancs indiquent un 4 et un 1. 
Max annonce clairement « cinq ».
Sur sa fiche, Emma coche le 5 de la rangée jaune.
Max coche le 5 rouge. Laura et Linus ne cochent aucun chiffre.
2. Le joueur actif (seulement lui !) peut – mais ne doit pas – additionner un dé blanc et n’importe quel autre dé de couleur de son choix et cocher la somme dans la rangée de la même couleur que le dé de couleur.
Exemple : Max additionne le 4 blanc et le 6 bleu et coche le chiffre 10 dans la rangée bleue.
Très important : au cas où le joueur actif ne coche aucune case, ni au cours de l’action 1, ni au cours de l’action 2, il doit faire une croix dans une case « coups
manqués ». 
Les autres joueurs ne doivent pas cocher cette case, n’importe qu’ils aient coché un chiffre ou non.
C’est maintenant au prochain joueur d’être le joueur actif (on joue dans le sens horaire). 
Il prend les six dés et les lance. Les deux actions ci-dessus sont ensuite exécutées l’une après l’autre. 
La partie se poursuit ainsi.

## Clôturer une rangée
Lorsqu’un joueur souhaite cocher le tout dernier chiffre d’une rangée (12 rouge, 12 jaune, 2 vert, 2 bleu), il doit avoir coché au moins cinq chiffres de cette rangée.
Si c’est le cas, il coche le dernier chiffre de la rangée et fait également une croix dans la case juste à côté, représentée par un cadenas – cette croix sera également additionnée lors du décompte final. 
Cette rangée est désormais clôturée pour tous les joueurs et aucun chiffre de la rangée de cette couleur ne pourra être coché au cours de la partie. 
Le dé correspondant est immédiatement retiré du jeu car il ne servira plus.
Exemple : Laura coche le 2 vert et le cadenas. 
Le dé vert est retiré du jeu.
Attention : lorsqu’un joueur coche le dernier chiffre d’une rangée, il doit l’annoncer clairement afin que tous les joueurs sachent que cette rangée est désormais clôturée. 
Si la clôture de la rangée a lieu pendant la action, d’autres joueurs peuvent éventuellement clôturer cette rangée en même temps, et également cocher le cadenas. 
Si, toutefois, un joueur a coché moins de 5 chiffres dans cette rangée, il ne peut en aucun cas cocher le cadenas, même si cette rangée est désormais clôturée.

## Fin de la partie
Le jeu se termine immédiatement lorsqu’un joueur a coché la quatrième case « coups manqués ». 
De plus, la partie se termine immédiatement lorsque deux rangées sont clôturées et que deux dés de couleur ont été retirés du jeu (n’importe par quel joueur).
Remarque : il peut arriver (pendant l’action 1), qu’en même temps que la deuxième rangée on puisse clôturer une troisième rangée.
Exemple : la rangée verte est déjà terminée. Emma lance les dés blancs qui indiquent deux 6. 
Elle annonce « 12 ». Max coche le 12 rouge et termine la rangée rouge. 
En même temps, Linus coche le 12 jaune et clôture également la rangée jaune.

## Décompte
En-dessous des quatre rangées de chiffres, on voit le nombre de points que l’on obtient pour les chiffres cochés dans chaque rangée. 
De plus, on soustrait 5 points pour chaque coup manqué. 
Les joueurs notent ensuite les points remportés pour chaque couleur en bas de leur fiche, sans oublier de soustraire les coups manqués.
Le joueur comptabilisant le plus de points remporte la partie.
Exemple : Laura a 4 croix dans la rangée rouge, cela fait 10 points, elle a 3 croix dans la rangée jaune (= 6 points), 7 croix dans la rangée verte (= 28 points) et 8 croix dans la rangée bleue (= 36 points). 
Elle a 10 points de pénalité pour ses deux coups manqués. 
Laura a donc un total de 70 points.
