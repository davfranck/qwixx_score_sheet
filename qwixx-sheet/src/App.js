import React, { useEffect, useState } from 'react';
import './App.css'; 
import LastRow from './components/lastrow/LastRow';
import AllColoredRows from './components/coloredRow/AllColoredRows';

function App() {
  const [total, setTotal] = useState(0)
  const [totalWithMisses, setTotalWithMisses] = useState(0)
  const [gameOver, setGameOver] = useState(false)
  const [misses, setMisses] = useState(0)
  const [showScore, setShowScore] = useState(true)

  useEffect(() => {
    if (misses === 4) {
      setGameOver(true)
    }
  
  }, [misses])

  useEffect(() => {
    setTotalWithMisses(total - (misses * 5))
  }, [misses, total])


  const totalUpdated = (total) => {
    setTotal(total)
  }

  const onMiss = () => {
    if(gameOver) {
      return false;
    }
    setMisses(misses +1);
    return true;
  }

  const onShowScore = () => {
    setShowScore(!showScore)
  }

  return (
    <div className="App">
      <AllColoredRows onTotalUpdated={totalUpdated} gameOver={gameOver} showScore={showScore} />
      <LastRow total={totalWithMisses} gameOver={gameOver} onMiss={onMiss} onShowScore={onShowScore} showScore={showScore}/>
      <br/>
    </div>
  );
}

export default App;

// question : total géré via contexte ? idem pour gameOver ?