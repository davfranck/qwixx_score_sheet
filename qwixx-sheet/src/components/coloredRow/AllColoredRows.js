import { useEffect, useState } from "react"
import TwoToTwelveRow from "./TwoToTwelveRow"
import TwelveToTwoRow from "./TwelveToTwoRow"


const AllColoredRows = ({onTotalUpdated, gameOver, showScore}) => {
    const [redScore, setRedScore] = useState(0)
    const [yellowScore, setYellowScore] = useState(0)
    const [greenScore, setGreenScore] = useState(0)
    const [blueScore, setBlueScore] = useState(0)
  
    useEffect(() => {
      const total = redScore + yellowScore + greenScore + blueScore;
      onTotalUpdated(total);
    }, [redScore, yellowScore, greenScore, blueScore, onTotalUpdated]);
  
    const rowUpdated = (color, colorTotal) => {
      switch (color) {
        case 'red':
          setRedScore(colorTotal)
          break
        case 'yellow':
          setYellowScore(colorTotal)
          break
        case 'green':
          setGreenScore(colorTotal)
          break
        case 'blue':
          setBlueScore(colorTotal)
          break
        default:
          break
      }
    }
    return (
      <>
        <TwoToTwelveRow color="red" onRowUpdated={rowUpdated} gameOver={gameOver} showScore={showScore}/>
        <TwoToTwelveRow color="yellow" onRowUpdated={rowUpdated} gameOver={gameOver} showScore={showScore}/>
        <TwelveToTwoRow color="green" onRowUpdated={rowUpdated} gameOver={gameOver} showScore={showScore}/>
        <TwelveToTwoRow color="blue" onRowUpdated={rowUpdated} gameOver={gameOver} showScore={showScore}/>
      </>
    )
  }

  export default AllColoredRows;