const LockSquare = ({onLockRow, isLocked}) => {
    const lockRow = () => {
        onLockRow()
    }
    return (
        <svg viewBox="0 0 100 100" className="case" onClick={lockRow}>
            <circle cx="50" cy="50" r="40" stroke="black" strokeWidth="3" fill="white" />
            <text x="50%" y="65%" textAnchor="middle" fill="red" fontFamily="FontAwesome" fontSize="50px">&#xf023;</text>
            {isLocked && <>
            <line x1="0" y1="0" x2="100" y2="100" style={{ stroke: 'red', strokeWidth: 2 }} />
            <line x1="100" y1="0" x2="0" y2="100" style={{ stroke: 'red', strokeWidth: 2 }} />
            </>}
        </svg>
    )
};

export default LockSquare;