const Score = ({value})  => {
    return (
      <svg viewBox="0 0 100 100" className="case">
          <rect width="100" height="100" rx="10" style={{ fill: "#e8dcba" }} />
          <text x="50" y="50" className="text">{value}</text>
      </svg>
      )
    };

export default Score;