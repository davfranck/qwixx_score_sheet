import React, { useEffect, useState } from 'react';
import SquareWithNumber from './SquareWithNumber';
import LockSquare from './LockSquare';
import Score from './Score';

const SCORE_BY_NUMBER_OF_CHECKED_SQUARE = {"0": 0, "1": 1, "2": 3, "3": 6, "4": 10, "5": 15, "6": 21, "7": 28, "8": 36, "9": 45, "10": 55, "11": 66, "12": 78 }

const TwelveToTwoRow = ({ color, onRowUpdated, gameOver, showScore }) => {
  const [checkedNumbers, setCheckNumbers] = useState([])
  const [total, setTotal] = useState(0)
  const [isLocked, setIsLocked] = useState(false)
  
  useEffect(() => {
    if(checkedNumbers.length > 5 && checkedNumbers[checkedNumbers.length - 1] === 2) {
      setTotal(SCORE_BY_NUMBER_OF_CHECKED_SQUARE[(checkedNumbers.length + 1).toString()])
    } else {
      setTotal(SCORE_BY_NUMBER_OF_CHECKED_SQUARE[checkedNumbers.length.toString()])
    }
  }, [checkedNumbers, color])

  useEffect(() => {
    onRowUpdated(color, total)
  }, [total, color, onRowUpdated])// question : pourquoi devoir mettre onRowUpdated ou même color ?

  const updateScore = (squareNumber) => {
    if(gameOver) {
      return false;
    }

    if(isLocked) {
      return false;
    }
    
    const alreadyChecked = checkedNumbers.includes(squareNumber)
    if(alreadyChecked) {
      return false;
    }

    const squareNumberIsBiggerThanMaxChecked = checkedNumbers[checkedNumbers.length - 1] < squareNumber
    if (squareNumberIsBiggerThanMaxChecked) {
      return false;
    }

    if (squareNumber === 2 && checkedNumbers.length < 5) {
      return false;
    }

    setCheckNumbers(previousState => previousState.concat([squareNumber]))

    if(squareNumber === 2) {
      setIsLocked(true)
    }
    
    return true;
  }
  
  const lockRow = () => {
    setIsLocked(true)
  }

  return (
    <div className="container" style={{backgroundColor: color}}>
        {Array(11).fill().map((_, i) => <SquareWithNumber key={i} number={12 - i} updateScore={updateScore}/>)}
        <LockSquare onLockRow={lockRow} isLocked={isLocked}/>
        {showScore && <Score value={total}/> }
    </div>
)};

export default TwelveToTwoRow;

// comment formatter le js