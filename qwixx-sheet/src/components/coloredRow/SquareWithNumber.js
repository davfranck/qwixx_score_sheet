import { useState } from "react";

const SquareWithNumber = ({ number, updateScore }) => {
    const [done, setDone] = useState(false);
    const onNumberChecked = () => {
      const updated = updateScore(number);
      if (updated) {
        setDone(true);
      }
    }
    return (
      <svg viewBox="0 0 100 100" className="case" onClick={onNumberChecked}>
        <rect width="100" height="100" rx="10" style={{ fill: "#e8dcba" }} />
        <text x="50" y="50" className="text">{number}</text>
        {done && <>
            <line x1="0" y1="0" x2="100" y2="100" style={{ stroke: 'red', strokeWidth: 2 }} />
            <line x1="100" y1="0" x2="0" y2="100" style={{ stroke: 'red', strokeWidth: 2 }} />
          </>}
      </svg>
    )
  };

export default SquareWithNumber;