const Compute = ({onShowScore}) => (
    <svg viewBox="0 0 100 100" className="case" onClick={() => onShowScore()}>
        <circle cx="50" cy="50" r="40" stroke="black" strokeWidth="3" fill="white" />
        <text x="50%" y="70%" textAnchor="middle" fill="red" fontFamily="FontAwesome" fontSize="50px">&#xf06e;</text>
    </svg>
);

export default Compute;