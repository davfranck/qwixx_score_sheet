import Compute from "./Compute";
import Misses from "./Misses";
import Total from "./Total";

const LastRow = ({total, onMiss, onShowScore, showScore}) => (
    <div className="container">
        <Misses onMiss={onMiss}/>
        <Compute onShowScore={onShowScore}/>
        {showScore && <Total value={total}/>}
    </div>
);

export default LastRow;