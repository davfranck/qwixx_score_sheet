import Miss from './Miss';

const Misses = ({onMiss}) => {
    return (
        <>
            <Miss onMiss={() => onMiss()}  />
            <Miss onMiss={() => onMiss()}  />
            <Miss onMiss={() => onMiss()}  />
            <Miss onMiss={() => onMiss()}  />
        </>
    )
}

export default Misses;