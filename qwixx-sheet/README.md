# README

Pour lancer le projet : `npm start`. 

Pour les test : `npm test`

Pour générer le build de prod : `npm run build`. 

## TODOs
* refacto avec contexte + objet "partie" avec état et méthodes
    => impact sur rechargement des composants ?
* nouvelle partie (galère sans état global !)
* copier coller TwoToTWelve et TwelveToTwo : factoriser
* pas de test
* griser le 12/2 quand on ne peut pas les cocher
* simplifier le déploiement

## Utile

* [doc react](https://react.dev/reference/react)
* [feuille de score qwixx](https://www.letempledujeu.fr/IMG/jpg/qwixx_spielblatt.jpg)